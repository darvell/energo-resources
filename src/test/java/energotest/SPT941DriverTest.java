package energotest;

import org.junit.Assert;
import org.junit.Test;
import ru.darvell.energo.devices.SPT941ArchData;
import ru.darvell.energo.driver.SPT941Driver;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class SPT941DriverTest {

    private static final double DELTA = 1e-15;

    @Test
    public void testConverters() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method methodToDouble = SPT941Driver.class.getDeclaredMethod("toDouble", String.class);
        methodToDouble.setAccessible(true);

        Method methodtoDoubleFromFlash = SPT941Driver.class.getDeclaredMethod("toDoubleFromFlash", String.class);
        methodtoDoubleFromFlash.setAccessible(true);

        SPT941Driver driver = new SPT941Driver();
        Assert.assertEquals((double)methodToDouble.invoke(driver, "6666667d"),0.44999998807907104d, DELTA);
        Assert.assertEquals((double)methodToDouble.invoke(driver, "00000000"),.0d, DELTA);
        Assert.assertEquals((double)methodtoDoubleFromFlash.invoke(driver, "988800006666667d"), 34968.44999998807907104, DELTA);
        Assert.assertEquals((double)methodtoDoubleFromFlash.invoke(driver, "0000000000000000"), .0, DELTA);
    }

    @Test
    public void testCommands(){
        SPT941Driver driver = new SPT941Driver();
        Assert.assertEquals(driver.getInitCommand(),"ffffffffffffffffff");
    }

    @Test
    public void testCrc() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method method = SPT941Driver.class.getDeclaredMethod("getCrc", String.class);
        method.setAccessible(true);

        SPT941Driver driver = new SPT941Driver();
        Assert.assertEquals(method.invoke(driver, "ff45d0070100"), 0xe3);
        Assert.assertEquals(method.invoke(driver, "ff453f420100"), 0x39);
        Assert.assertEquals(method.invoke(driver, "ff453f424000"), 0xfa);
    }

    @Test
    public void testReadFlashCommand(){
        SPT941Driver driver = new SPT941Driver();
        Assert.assertEquals(driver.getReadFlashCommand(0,1), "10ff4500000100ba16");
        Assert.assertEquals(driver.getReadFlashCommand(999999,1), "10ff453f4201003916");
        Assert.assertEquals(driver.getReadFlashCommand(999999,64), "10ff453f424000fa16");
        Assert.assertEquals(driver.getReadFlashCommand(999999,10000).length(), 18);

    }

    @Test
    public void archDataParserTest(){
        String archData = "10ff4800000005eb9f11859ab44384e17a34822e3a3282000000006a3a30826a3a308200000000b0d7067d0000007f00000000000000000000000000000000000000001c16";
        SPT941ArchData spt941ArchData = new SPT941ArchData();
        spt941ArchData.setSp(5);
        spt941ArchData.setT1(72.81233978271484d);
        spt941ArchData.setT2(48.926368713378906d);
        spt941ArchData.setV1(11.279999732971191d);
        spt941ArchData.setV2(11.139204025268555d);
        spt941ArchData.setV3(.0d);
        spt941ArchData.setM1(11.014261245727539d);
        spt941ArchData.setM2(11.014261245727539d);
        spt941ArchData.setM3(.0d);
        spt941ArchData.setQ(0.263364315032959d);
        spt941ArchData.setTi(1.0d);

        SPT941Driver driver = new SPT941Driver();
        Assert.assertEquals(driver.parseArchData(archData), spt941ArchData);
    }
}
