package ru.darvell.energo.devices;

public class SPT941ArchData {

    private int sp;
    private double t1;
    private double t2;
    private double V1;
    private double V2;
    private double V3;
    private double M1;
    private double M2;
    private double M3;
    private double Q;
    private double Ti;

    public int getSp() {
        return sp;
    }

    public void setSp(int sp) {
        this.sp = sp;
    }

    public double getT1() {
        return t1;
    }

    public void setT1(double t1) {
        this.t1 = t1;
    }

    public double getT2() {
        return t2;
    }

    public void setT2(double t2) {
        this.t2 = t2;
    }

    public double getV1() {
        return V1;
    }

    public void setV1(double v1) {
        V1 = v1;
    }

    public double getV2() {
        return V2;
    }

    public void setV2(double v2) {
        V2 = v2;
    }

    public double getV3() {
        return V3;
    }

    public void setV3(double v3) {
        V3 = v3;
    }

    public double getM1() {
        return M1;
    }

    public void setM1(double m1) {
        M1 = m1;
    }

    public double getM2() {
        return M2;
    }

    public void setM2(double m2) {
        M2 = m2;
    }

    public double getM3() {
        return M3;
    }

    public void setM3(double m3) {
        M3 = m3;
    }

    public double getQ() {
        return Q;
    }

    public void setQ(double q) {
        Q = q;
    }

    public double getTi() {
        return Ti;
    }

    public void setTi(double ti) {
        Ti = ti;
    }

    @Override
    public String toString() {
        return "SPT941ArchData{" +
                "sp=" + sp +
                ", t1=" + t1 +
                ", t2=" + t2 +
                ", V1=" + V1 +
                ", V2=" + V2 +
                ", V3=" + V3 +
                ", M1=" + M1 +
                ", M2=" + M2 +
                ", M3=" + M3 +
                ", Q=" + Q +
                ", Ti=" + Ti +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SPT941ArchData that = (SPT941ArchData) o;

        if (sp != that.sp) return false;
        if (Double.compare(that.t1, t1) != 0) return false;
        if (Double.compare(that.t2, t2) != 0) return false;
        if (Double.compare(that.V1, V1) != 0) return false;
        if (Double.compare(that.V2, V2) != 0) return false;
        if (Double.compare(that.V3, V3) != 0) return false;
        if (Double.compare(that.M1, M1) != 0) return false;
        if (Double.compare(that.M2, M2) != 0) return false;
        if (Double.compare(that.M3, M3) != 0) return false;
        if (Double.compare(that.Q, Q) != 0) return false;
        return Double.compare(that.Ti, Ti) == 0;

    }
}
