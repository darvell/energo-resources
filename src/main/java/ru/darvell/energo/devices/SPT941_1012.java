package ru.darvell.energo.devices;

import java.util.List;

public class SPT941_1012 {

    private long id;
    private String host;
    private String port;

    private List<SPT941ArchData> archDatas;

    public SPT941_1012() {
    }

    public SPT941_1012(String host, String port) {
        this.host = host;
        this.port = port;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public List<SPT941ArchData> getArchDatas() {
        return archDatas;
    }

    public void setArchDatas(List<SPT941ArchData> archDatas) {
        this.archDatas = archDatas;
    }
}
