package ru.darvell.energo;

import ru.darvell.energo.devices.SPT941ArchData;
import ru.darvell.energo.driver.SPT941Driver;

import java.time.LocalDateTime;
import java.util.List;

public class Main {

    public static void main(String[] args){

        int port = 4001;
        String host = "192.168.48.2";


        SPT941Driver spt941Driver = new SPT941Driver();

        try{

            LocalDateTime start = LocalDateTime.of(2015,11,30,0,0);
            LocalDateTime stop = LocalDateTime.now();

            List<SPT941ArchData> data = spt941Driver.readArchives(SPT941Driver.ARCH_HOUR, start, stop);
            data.stream().forEach(System.out::println);
        }catch(Exception e){
            e.printStackTrace();
        }



    }


}
