package ru.darvell.energo.driver.utils;

public class DigitWorks {

    public static byte[] toByteArray(String s){
        if (s.length()%2 != 0){
            s = "0"+s;
        }
        byte[] data = new byte[s.length()/2];
        for (int i=0; i<s.length();i+=2){
            data[i/2] = (byte) ((Character.digit(s.charAt(i),16) << 4)+(Character.digit(s.charAt(i+1),16)));
        }
        return data;
    }

    public static long getMaskForXor(long number){
        int bitCount = Long.toBinaryString(number).length();
        StringBuilder sb = new StringBuilder();
        for (int i = 0;i<bitCount;i++){
            sb.append("1");
        }
        long mask = Long.parseLong(sb.toString(),2);
        return mask;
    }

}
