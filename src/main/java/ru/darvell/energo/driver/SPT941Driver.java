package ru.darvell.energo.driver;

import ru.darvell.energo.devices.SPT941ArchData;
import ru.darvell.energo.driver.utils.DigitWorks;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class SPT941Driver {

    public final static int ARCH_HOUR = 1;
    public final static int ARCH_DAY = 2;
    public final static int ARCH_MONTH = 3;

    final String firsByteSTR = "10";
    final String lastByteSTR = "16";

    int port = 4001;
    String host = "192.168.48.2";

    public String getInitCommand(){
        return "ffffffffffffffffff";
    }

    public String getFirstCommand(){
        return "10ff3f00000000c116";
    }

    public String getReadFlashCommand(int pageNumber, int pageCount){
        StringBuilder sbResult  = new StringBuilder();
        StringBuilder sbData  = new StringBuilder();

        int mask = 0b11111111;
        int smallBit = pageNumber & mask;
        pageNumber = pageNumber >> 8;
        int bigBit = pageNumber & mask;

        sbData.append("ff");
        sbData.append("45");
        sbData.append(String.format("%02x", smallBit));
        sbData.append(String.format("%02x", bigBit));
        sbData.append(String.format("%02x", pageCount & 0xff));

        sbData.append("00");

        long crcSumm = getCrc(sbData.toString());

        sbResult.append(firsByteSTR);
        sbResult.append(sbData);
        sbResult.append(String.format("%02x", crcSumm));
        sbResult.append(lastByteSTR);
        return sbResult.toString();
    }

    public String getArchiveCommand(int gg, int mm, int dd, int hh){
        StringBuilder sbData = new StringBuilder();
        StringBuilder sbResult  = new StringBuilder();

        sbData.append("ff");

        if (hh == 0) {
            if (dd == 0){
                sbData.append("4d");
            }else {
                sbData.append("59");
            }
        }else{
            sbData.append("48");
        }


        sbData.append(String.format("%02x", gg-2000+100));
        sbData.append(String.format("%02x", mm));
        sbData.append(String.format("%02x", dd));
        sbData.append(String.format("%02x", hh));

        long crcSumm = getCrc(sbData.toString());
        sbResult.append(firsByteSTR);
        sbResult.append(sbData);
        sbResult.append(String.format("%02X", 0xFF & crcSumm));
        sbResult.append(lastByteSTR);
        return sbResult.toString();
    }

    public String getRamCommand(int startAddr, int count){
        int mask = 0b11111111;
        int smallBit = startAddr & mask;
        startAddr = startAddr >> 8;
        int bigBit = startAddr & mask;

        StringBuilder sbData = new StringBuilder();
        StringBuilder sbResult  = new StringBuilder();
        sbData.append("ff");
        sbData.append("52");
        sbData.append(String.format("%02x", smallBit));
        sbData.append(String.format("%02x", bigBit));
        sbData.append(String.format("%02x", count));
        sbData.append("00");

        long crcSumm = getCrc(sbData.toString());
        sbResult.append(firsByteSTR);
        sbResult.append(sbData.toString());
        sbResult.append(String.format("%02x", crcSumm));
        sbResult.append(lastByteSTR);
        return sbResult.toString();
    }

    public SPT941ArchData parseArchData(String s){
        if (s.substring(4,6).equals("48") || s.substring(4,6).equals("59") || s.substring(4,6).equals("4d")) {
            SPT941ArchData data = new SPT941ArchData();
            data.setSp(Integer.parseInt(s.substring(13, 14), 16));
            data.setT1(toDouble(s.substring(14, 22)));
            data.setT2(toDouble(s.substring(22, 30)));
            data.setV1(toDouble(s.substring(30, 38)));
            data.setV2(toDouble(s.substring(38, 46)));
            data.setV3(toDouble(s.substring(46, 54)));
            data.setM1(toDouble(s.substring(54, 62)));
            data.setM2(toDouble(s.substring(62, 70)));
            data.setM3(toDouble(s.substring(70, 78)));
            data.setQ(toDouble(s.substring(78, 86)));
            data.setTi(toDouble(s.substring(86, 94)));
            return data;
        }else {
            return null;
        }
    }

    public void parseRamData(String s){
        System.out.println(s.substring(12,14));
        System.out.println(toDouble(s.substring(14,22)));
        System.out.println(toDouble(s.substring(22,30)));
        System.out.println(toDouble(s.substring(30,38)));
        System.out.println(toDouble(s.substring(38,46)));
        System.out.println(toDouble(s.substring(46,54)));
        System.out.println(toDouble(s.substring(54,62)));
    }

    private int getCrc(String bytes){
        long summ = 0;
        for(int i=0; i<bytes.length(); i+=2){
            summ += Long.parseLong(bytes.substring(i,i+2),16);
        }
        long mask = DigitWorks.getMaskForXor(summ);
        return (int)(summ ^ mask) & 0xff;
    }


    /**
     * Преобразование в дробное число по арифметике для SPT
     * В качестве знака не старший бит чсила, а старший бит мантиссы
     * @param hexVal Hex строка 4 бита
     * @return Преобразованное число
     */

    private double toDouble(String hexVal){
        StringBuilder sb = new StringBuilder();
        for (int i=hexVal.length();i>1;i-=2){
            sb.append(hexVal.substring(i-2,i));
        }
        long tmp = Long.parseLong(sb.toString(),16);

        if (tmp == 0){return .0d;}

        String binary = Long.toBinaryString(tmp);
        String mantissa = binary.substring(binary.length()-24, binary.length());
        String poradok = binary.substring(0, binary.length()-24);
        int a = Integer.parseInt(mantissa.substring(0,1));
        mantissa = "1"+mantissa.substring(1,mantissa.length());

        double summ = 0;
        for (int i=0; i<24;i++){
            double summ_d = Double.parseDouble(mantissa.substring(i,i+1));
            double summ_a = summ_d*2;
            double summ_b = i*(-1);
            if (summ_d==1) {
                summ += Math.pow(summ_a, summ_b);
            }
        }
        double result = Math.pow(-1,a)*summ*Math.pow(2,Integer.parseInt(poradok,2)-127);
        return result;
    }


    /**
     * Преобрадование в double hex числа из flash памяти прибора
     * 4 байта - целая часть Integer
     * 4 байта - дробная часть FLOAT (SPT941)
     * @param hexVal
     * @return
     */

    private double toDoubleFromFlash(String hexVal){
        String fractionPartHex = hexVal.substring(8,16);
        StringBuilder sb = new StringBuilder();
        for (int i=hexVal.length();i>1;i-=2){
            sb.append(hexVal.substring(i-2,i));
        }
        String intPartHex = sb.toString().substring(8,16);
        return Integer.parseInt(intPartHex,16)+toDouble(fractionPartHex);
    }

    public List<SPT941ArchData> readArchives(int archType, LocalDateTime startDate, LocalDateTime stopDate) throws IOException, InterruptedException {

        List<SPT941ArchData> result = new ArrayList<>();

        Socket socket = new Socket(host,port);
        InputStream inputStream = socket.getInputStream();
        OutputStream outputStream = socket.getOutputStream();

        outputStream.write(DigitWorks.toByteArray(getInitCommand()));
        Thread.sleep(1000);
        outputStream.write(DigitWorks.toByteArray(getFirstCommand()));
        Thread.sleep(2200);
        readFromStream(inputStream);

        while (startDate.compareTo(stopDate) < 0){
            String archCommand = "";
            switch (archType){
                case ARCH_DAY:startDate = startDate.plusDays(1);
                     archCommand = getArchiveCommand(startDate.getYear(),startDate.getMonthValue(),startDate.getDayOfMonth(),0);
                    break;
                case ARCH_HOUR:startDate = startDate.plusHours(1);
                    archCommand = getArchiveCommand(startDate.getYear(),startDate.getMonthValue(),startDate.getDayOfMonth(),startDate.getHour()+1);
                    break;
                case ARCH_MONTH:startDate = startDate.plusMonths(1);
                    archCommand = getArchiveCommand(startDate.getYear(),startDate.getMonthValue(),0,0);
                    break;
            }
            outputStream.write(DigitWorks.toByteArray(archCommand));
            Thread.sleep(2200);
            String rawDataStr = readFromStream(inputStream);
            SPT941ArchData archData = parseArchData(rawDataStr);
            if (archData != null){
                result.add(archData);
            }
        }
        inputStream.close();
        outputStream.close();
        socket.close();
        return result;
    }

    private String readFromStream(InputStream inputStream) throws IOException {
        byte buffer[] = new byte[1024*16];
        StringBuilder sb = new StringBuilder();
        while (inputStream.available()>0){
            int count = inputStream.read(buffer);
            for (int i=0; i<count;i++){
                sb.append(String.format("%02x", buffer[i]));
            }
        }
        return sb.toString();
    }
}
